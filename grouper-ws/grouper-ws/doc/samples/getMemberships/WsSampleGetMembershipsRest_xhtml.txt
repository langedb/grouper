Grouper web service sample of service: getMemberships, WsSampleGetMembershipsRest, manually written lite/rest, format: xhtml, for version: 2.1.1


#########################################
##
## HTTP request sample (could be formatted for view by
## indenting or changing dates or other data)
##
#########################################


POST /grouper-ws/servicesRest/v2_1_001/memberships HTTP/1.1
Connection: close
Authorization: Basic xxxxxxxxxxxxxxxxx==
User-Agent: Jakarta Commons-HttpClient/3.1
Host: localhost:8092
Content-Length: 861
Content-Type: application/xhtml+xml; charset=UTF-8

<?xml version='1.0' encoding='iso-8859-1'?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>WsRestGetMembershipsRequest</title>
  </head>
  <body>
    <div title="WsRestGetMembershipsRequest">
      <p class="scope" />
      <p class="enabled" />
      <p class="stemScope" />
      <div class="wsStemLookup" title="WsStemLookup" />
      <p class="clientVersion" />
      <ul class="wsGroupLookups">
        <li title="WsGroupLookup">
          <p class="uuid" />
          <p class="groupName">aStem:aGroup</p>
        </li>
      </ul>
      <p class="memberFilter" />
      <div class="actAsSubjectLookup" title="WsSubjectLookup" />
      <p class="includeGroupDetail" />
      <p class="includeSubjectDetail" />
      <ul class="subjectAttributeNames">
        <li>description</li>
        <li>loginid</li>
        <li>name</li>
      </ul>
      <p class="fieldName" />
    </div>
  </body>
</html>


#########################################
##
## HTTP response sample (could be formatted for view by
## indenting or changing dates or other data)
##
#########################################


HTTP/1.1 200 OK
Server: Apache-Coyote/1.1
Set-Cookie: JSESSIONID=xxxxxxxxxxxxxxxxxxxxxxxx; Path=/grouper-ws
X-Grouper-resultCode: SUCCESS
X-Grouper-success: T
X-Grouper-resultCode2: NONE
Content-Type: application/xhtml+xml;charset=UTF-8
Date: Sun, 01 Jul 2012 15:46:56 GMT
Connection: close

<?xml version='1.0' encoding='iso-8859-1'?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>WsGetMembershipsResults</title>
  </head>
  <body>
    <div title="WsGetMembershipsResults">
      <div class="resultMetadata" title="WsResultMeta">
        <p class="resultCode">SUCCESS</p>
        <p class="resultCode2" />
        <p class="resultMessage">Found 3 results involving 1 groups and 3 subjects</p>
        <p class="success">T</p>
      </div>
      <div class="responseMetadata" title="WsResponseMeta">
        <p class="millis">670</p>
        <p class="resultWarnings" />
        <p class="serverVersion">2.1.1</p>
      </div>
      <ul class="subjectAttributeNames">
        <li>description</li>
        <li>loginid</li>
        <li>name</li>
      </ul>
      <ul class="wsSubjects">
        <li title="WsSubject">
          <p class="name">GrouperSysAdmin</p>
          <p class="id">GrouperSystem</p>
          <p class="sourceId">g:isa</p>
          <ul class="attributeValues">
            <li>GrouperSysAdmin</li>
            <li />
            <li>EveryEntity</li>
          </ul>
          <p class="resultCode">SUCCESS</p>
          <p class="success">T</p>
          <p class="identifierLookup" />
        </li>
        <li title="WsSubject">
          <p class="name">10021368</p>
          <p class="id">10021368</p>
          <p class="sourceId">jdbc</p>
          <ul class="attributeValues">
            <li />
            <li />
            <li>10021368</li>
          </ul>
          <p class="resultCode">SUCCESS</p>
          <p class="success">T</p>
          <p class="identifierLookup" />
        </li>
        <li title="WsSubject">
          <p class="name">10039438</p>
          <p class="id">10039438</p>
          <p class="sourceId">jdbc</p>
          <ul class="attributeValues">
            <li />
            <li />
            <li>10039438</li>
          </ul>
          <p class="resultCode">SUCCESS</p>
          <p class="success">T</p>
          <p class="identifierLookup" />
        </li>
      </ul>
      <ul class="wsGroups">
        <li title="WsGroup">
          <p class="name">aStem:aGroup</p>
          <p class="displayName">a stem:a group</p>
          <p class="description">a group description</p>
          <p class="uuid">674d142dd8084e338c1308b0abb798b2</p>
          <div class="detail" title="WsGroupDetail" />
          <p class="extension">aGroup</p>
          <p class="displayExtension">a group</p>
          <p class="typeOfGroup">group</p>
        </li>
      </ul>
      <ul class="wsMemberships">
        <li title="WsMembership">
          <p class="subjectSourceId">g:isa</p>
          <p class="subjectId">GrouperSystem</p>
          <p class="groupName">aStem:aGroup</p>
          <p class="createTime">2012/07/01 11:46:52.917</p>
          <p class="disabledTime" />
          <p class="enabled">T</p>
          <p class="enabledTime" />
          <p class="membershipId">c807fd350b3942869d8ef6937ee1f2eb:1d9db408dc324648aad7748d7469e46a</p>
          <p class="memberId">c6292ab1fe8c40289b69633fad5278f9</p>
          <p class="immediateMembershipId">c807fd350b3942869d8ef6937ee1f2eb</p>
          <p class="groupId">674d142dd8084e338c1308b0abb798b2</p>
          <p class="listName">members</p>
          <p class="listType">list</p>
          <p class="membershipType">immediate</p>
        </li>
        <li title="WsMembership">
          <p class="subjectSourceId">jdbc</p>
          <p class="subjectId">10021368</p>
          <p class="groupName">aStem:aGroup</p>
          <p class="createTime">2012/07/01 11:46:52.990</p>
          <p class="disabledTime" />
          <p class="enabled">T</p>
          <p class="enabledTime" />
          <p class="membershipId">6d13edc29b2e44a2a93ada8cf31cf73b:1d9db408dc324648aad7748d7469e46a</p>
          <p class="memberId">73887c79c97d4ee5903411f208be2bd8</p>
          <p class="immediateMembershipId">6d13edc29b2e44a2a93ada8cf31cf73b</p>
          <p class="groupId">674d142dd8084e338c1308b0abb798b2</p>
          <p class="listName">members</p>
          <p class="listType">list</p>
          <p class="membershipType">immediate</p>
        </li>
        <li title="WsMembership">
          <p class="subjectSourceId">jdbc</p>
          <p class="subjectId">10039438</p>
          <p class="groupName">aStem:aGroup</p>
          <p class="createTime">2012/07/01 11:46:52.963</p>
          <p class="disabledTime" />
          <p class="enabled">T</p>
          <p class="enabledTime" />
          <p class="membershipId">10a1636cb25146809b335d480c929a58:1d9db408dc324648aad7748d7469e46a</p>
          <p class="memberId">124e34436b424b8dbe476c1ec1979270</p>
          <p class="immediateMembershipId">10a1636cb25146809b335d480c929a58</p>
          <p class="groupId">674d142dd8084e338c1308b0abb798b2</p>
          <p class="listName">members</p>
          <p class="listType">list</p>
          <p class="membershipType">immediate</p>
        </li>
      </ul>
    </div>
  </body>
</html>


#########################################
##
## Java source code (note, any programming language / objects
## can use used to generate the above request/response.  Nothing
## is Java specific.  Also, if you are using Java, the client libraries
## are available
##
#########################################


/*******************************************************************************
 * Copyright 2012 Internet2
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package edu.internet2.middleware.grouper.ws.samples.rest.membership;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.DefaultHttpParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;

import edu.internet2.middleware.grouper.ws.coresoap.WsGetMembershipsResults;
import edu.internet2.middleware.grouper.ws.coresoap.WsGroupLookup;
import edu.internet2.middleware.grouper.ws.rest.WsRestResultProblem;
import edu.internet2.middleware.grouper.ws.rest.membership.WsRestGetMembershipsRequest;
import edu.internet2.middleware.grouper.ws.samples.types.WsSampleRest;
import edu.internet2.middleware.grouper.ws.samples.types.WsSampleRestType;
import edu.internet2.middleware.grouper.ws.util.RestClientSettings;

/**
 * @author mchyzer
 */
public class WsSampleGetMembershipsRest implements WsSampleRest {

  /**
   * get member lite web service with REST
   * @param wsSampleRestType is the type of rest (xml, xhtml, etc)
   */
  public static void getMembershipsLite(WsSampleRestType wsSampleRestType) {

    try {
      HttpClient httpClient = new HttpClient();
      
      DefaultHttpParams.getDefaultParams().setParameter(
          HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));

      //URL e.g. http://localhost:8093/grouper-ws/servicesRest/v1_3_000/...
      //NOTE: aStem:aGroup urlencoded substitutes %3A for a colon
      String url = RestClientSettings.URL + "/" + RestClientSettings.VERSION  
        + "/memberships";
      PostMethod method = new PostMethod(
          url);

      httpClient.getParams().setAuthenticationPreemptive(true);
      Credentials defaultcreds = new UsernamePasswordCredentials(RestClientSettings.USER, 
          RestClientSettings.PASS);

      //no keep alive so response if easier to indent for tests
      method.setRequestHeader("Connection", "close");
      
      //e.g. localhost and 8093
      httpClient.getState()
          .setCredentials(new AuthScope(RestClientSettings.HOST, RestClientSettings.PORT), defaultcreds);

      //Make the body of the request, in this case with beans and marshaling, but you can make
      //your request document in whatever language or way you want
      WsRestGetMembershipsRequest getMemberships = new WsRestGetMembershipsRequest();

      // seeif two subjects are in the group
      WsGroupLookup[] groupLookups = new WsGroupLookup[1];
      groupLookups[0] = new WsGroupLookup("aStem:aGroup", null);

      getMemberships.setWsGroupLookups(groupLookups);

      getMemberships.setSubjectAttributeNames(new String[]{"description", "loginid", "name"});
      
      //get the xml / json / xhtml / paramString
      String requestDocument = wsSampleRestType.getWsLiteRequestContentType().writeString(getMemberships);
      
      //make sure right content type is in request (e.g. application/xhtml+xml
      String contentType = wsSampleRestType.getWsLiteRequestContentType().getContentType();
      
      method.setRequestEntity(new StringRequestEntity(requestDocument, contentType, "UTF-8"));
      
      httpClient.executeMethod(method);

      //make sure a request came back
      Header successHeader = method.getResponseHeader("X-Grouper-success");
      String successString = successHeader == null ? null : successHeader.getValue();
      if (StringUtils.isBlank(successString)) {
        throw new RuntimeException("Web service did not even respond!");
      }
      boolean success = "T".equals(successString);
      String resultCode = method.getResponseHeader("X-Grouper-resultCode").getValue();
      
      String response = RestClientSettings.responseBodyAsString(method);

      Object result = wsSampleRestType
        .getWsLiteResponseContentType().parseString(response);
      
      //see if problem
      if (result instanceof WsRestResultProblem) {
        throw new RuntimeException(((WsRestResultProblem)result).getResultMetadata().getResultMessage());
      }
      
      //convert to object (from xhtml, xml, json, etc)
      WsGetMembershipsResults wsGetMembershipsResults = (WsGetMembershipsResults)result;
      
      String resultMessage = wsGetMembershipsResults.getResultMetadata().getResultMessage();

      // see if request worked or not
      if (!success) {
        throw new RuntimeException("Bad response from web service: successString: " + successString + ", resultCode: " + resultCode
            + ", " + resultMessage);
      }
      
      System.out.println("Server version: " + wsGetMembershipsResults.getResponseMetadata().getServerVersion()
          + ", result code: " + resultCode
          + ", result message: " + resultMessage );

    } catch (Exception e) {
      throw new RuntimeException(e);
    }

  }

  /**
   * @param args
   */
  public static void main(String[] args) {
    getMembershipsLite(WsSampleRestType.xml);
  }

  /**
   * @see edu.internet2.middleware.grouper.ws.samples.types.WsSampleRest#executeSample(edu.internet2.middleware.grouper.ws.samples.types.WsSampleRestType)
   */
  public void executeSample(WsSampleRestType wsSampleRestType) {
    getMembershipsLite(wsSampleRestType);
  }

  /**
   * @see edu.internet2.middleware.grouper.ws.samples.types.WsSampleRest#validType(edu.internet2.middleware.grouper.ws.samples.types.WsSampleRestType)
   */
  public boolean validType(WsSampleRestType wsSampleRestType) {
    //dont allow http params
    return !WsSampleRestType.http_json.equals(wsSampleRestType);
  }
}


#########################################
##
## Stdout
##
#########################################


Server version: 2.1.1, result code: SUCCESS, result message: Found 3 results involving 1 groups and 3 subjects
