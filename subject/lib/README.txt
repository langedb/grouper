
This file provides information on the third party libraries that ship
with *Subject*.

---

# i2mi-common-0.1.0.jar

"i2mi-common is container for shared resources across Internet2 Middleware
projects."

Contains:
* cglib-nodep-2.1_03.jar
* commons-beanutils-1.7.0.jar
* commons-collections-3.2.jar
* commons-dbcp-1.2.1.jar
* commons-digester-1.7.jar
* commons-logging-1.1.jar
* commons-pool-1.3.jar
* dom4j-1.6.1.jar
* junit-4.1.jar
* odmg-3.0.jar
* p6spy.jar

* Version:  0.1.0
* Source:   <http://viewvc.internet2.edu/viewvc.py/i2mi-common/?root=I2MI>
* License:  Apache License, Version 2.0
* Updated:  20060807

---

$Id: README.txt,v 1.2 2008-10-13 08:04:29 mchyzer Exp $

