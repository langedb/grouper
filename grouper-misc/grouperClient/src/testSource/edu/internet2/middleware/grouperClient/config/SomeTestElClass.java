package edu.internet2.middleware.grouperClient.config;

/**
 * some test class for EL
 * @author mchyzer
 *
 */
public class SomeTestElClass {

  /**
   * some method for EL
   * @param a
   * @param b
   * @return the result
   */
  public static String someMethod(String a, String b) {
    return a + b + " something else";
  }
  
}
